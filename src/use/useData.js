import { ref } from "vue";

export default function useData() {
   const posts = ref([
      {
         id: 1,
         title: "welcome",
         body:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus sapiente ducimus repudiandae quod ipsa repellat provident eos aut, beatae recusandae libero molestias id esse tempore quo quidem animi ratione doloremque",
         tags: ["webdev", "coding", "news"],
      },
      {
         id: 2,
         title: "top 5 css tips",
         body:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus sapiente ducimus repudiandae quod ipsa repellat provident eos aut, beatae recusandae libero molestias id esse tempore quo quidem animi ratione doloremque",
         tags: ["css", "webdev", "coding"],
      },
   ]);

   function addData() {
      posts.value.push({
         id: posts.value.length,
         title: "welcome",
         body:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus sapiente ducimus repudiandae quod ipsa repellat provident eos aut, beatae recusandae libero molestias id esse tempore quo quidem animi ratione doloremque",
         tags: ["webdev", "coding", "news"],
      });
   }

   function removeData() {
      posts.value.pop();
   }

   return {
      posts,
      addData,
      removeData,
   };
}
